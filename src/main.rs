#[macro_use]
extern crate prettytable;
use std::collections::HashMap;
//use std::io;
//use std::io::stdin;
use std::io::stdout;
use std::io::Cursor;
use std::io::Read;
use std::io::Write;
use std::net::UdpSocket;
use std::path::Path;
use std::str;
//use std::sync::mpsc;
//use std::sync::mpsc::Receiver;
//use std::thread;
use std::time::Duration;
use std::time::Instant; //Instant

use byteorder::{LittleEndian, ReadBytesExt};
use cgmath::{Rotation, Vector3};
//use crossterm::cursor as term_cursor;
use crossterm::cursor::MoveTo;
use crossterm::event::Event;
use crossterm::event::KeyCode;
//use crossterm::event::KeyEvent;
use crossterm::event::KeyModifiers;
use crossterm::ExecutableCommand;
//Quaternion
use ipipe::OnCleanup;
use ipipe::Pipe;

mod tau_structs;
use tau_structs::DataPacket;
mod opengloves_structs;
use opengloves_structs::InputData;
mod helpers;
use crate::helpers::*;

const PACKET_SIZE: usize = 4096;

fn print_controls() {
    ptable!(
        [
            "Controls", "JOY", "A btn", "B btn", "JOY btn", "TRG btn", "GRAB", "PINCH", "MENU",
            "CALIB"
        ],
        [
            "Left Controller",
            "WASD",
            "E",
            "R",
            "F",
            "Q",
            "Z",
            "X",
            "C",
            "["
        ],
        [
            "Right Controller",
            "IJKL",
            "U",
            "Y",
            "H",
            "O",
            ",",
            "M",
            "N",
            "]"
        ]
    );
    ptable!(
        [
            "Print outputs",
            "Grab Calibration",
            "Print controls",
            "Toggle gestures",
            "Quit"
        ],
        ["T", "B", "G", "Space", "Esc / Ctrl+C"]
    );
}
fn main() {
    let uv_snap_angle: f32 = 70.0;
    let mut print_inputs_bool = false;
    let mut calibration_mode_bool = false;
    let mut gestures_enabled_bool = true;

    print_controls();

    let mut event_history: Vec<(Event, Instant)> = Vec::new();
    enable_raw_term();

    let mut left_hand_pipe = Pipe::open(
        Path::new(r"\\.\pipe\vrapplication\input\glove\v2\left"),
        OnCleanup::Delete,
    )
    .unwrap();
    let mut right_hand_pipe = Pipe::open(
        Path::new(r"\\.\pipe\vrapplication\input\glove\v2\right"),
        OnCleanup::Delete,
    )
    .unwrap();

    loop {
        // set local socket to receive data packets (port 0 means random open one)
        let socket_data_local = UdpSocket::bind("0.0.0.0:0").expect("Could not bind socket");
        // set socket read and write timeouts
        socket_data_local
            .set_read_timeout(Some(Duration::from_millis(1000)))
            .expect("set_read_timeout call failed");
        socket_data_local
            .set_write_timeout(Some(Duration::from_millis(1000)))
            .expect("set_read_timeout call failed");

        // create 'start receiving data' payload from string
        let drq_payload_bytes = b"drq101";
        // connect to remote socket to send packet and start receiving data
        match socket_data_local.connect("ttracker.local:19920") {
            Ok(_) => (),
            Err(_) => {
                eprintln!("connect function failed");
                continue;
            }
        };

        let mut packet_counter = -1;
        let mut drq_response_received = false;
        let mut cached_mapping: HashMap<u16, std::string::String> = HashMap::new();
        let mut last_packed_id: u16 = 0;
        let mut last_page: u8 = 0;
        let mut datapacket: DataPacket = DataPacket::empty();
        let mut multipage_buf: Vec<u8> = Vec::new();

        loop {
            if packet_counter == -1 || packet_counter > 500 {
                packet_counter = 0;
                // prepare buffer to read response
                let mut buf = [0; PACKET_SIZE];
                // send packet
                match socket_data_local.send(drq_payload_bytes) {
                    Ok(_) => (),
                    Err(_) => {
                        eprintln!("couldn't send message");
                        break;
                    }
                };
                // read response to sended packet
                if drq_response_received == false {
                    // read response to sended packet
                    let (_number_of_bytes, _src_addr) = match socket_data_local.recv_from(&mut buf)
                    {
                        Ok(val) => val,
                        Err(_) => {
                            eprintln!("Didn't receive data");
                            break;
                        }
                    };
                    // convert bytes to string and trim null bytes
                    let buf_txt = str::from_utf8(&buf).unwrap();
                    let buf_txt = buf_txt.trim_matches(char::from(0));
                    // assert that response is "ok"
                    assert_eq!(&buf_txt[..2], "ok");
                    println!("{}", buf_txt);
                    drq_response_received = true;
                }
            } else {
                packet_counter += 1;
            }

            // prepare buffer to read datapacket
            let mut buffer = [0; PACKET_SIZE];
            let (_number_of_bytes, _src_addr) = match socket_data_local.recv_from(&mut buffer) {
                Ok(val) => val,
                Err(_) => {
                    eprintln!("Didn't receive data");
                    break;
                }
            };

            // start measuring loop time
            // let now = Instant::now();

            let mut packetver = -1;
            if &buffer[..2] == b"dp" {
                packetver = 0;
            } else if &buffer[..2] == b"a2" {
                packetver = 1;
            }
            //println!("{}", packetver);

            if packetver == 1 {
                let mut cur = Cursor::new(&buffer[2..]);

                let _packet_id = cur.read_u16::<LittleEndian>().unwrap();

                let _current_page = cur.read_u8().unwrap();
                let _total_pages = cur.read_u8().unwrap();

                let mut skip_parsing = false;
                let mut parse_multipage = false;

                if _total_pages != 1 {
                    if last_packed_id != _packet_id && _current_page == 0 {
                        cur.read_to_end(&mut multipage_buf).unwrap();
                        skip_parsing = true;
                    } else if last_packed_id == _packet_id {
                        if _current_page != last_page + 1 {
                            println!(
                                "!page lost, id: {}, page: {}/{}",
                                _packet_id, _current_page, _total_pages
                            );
                            multipage_buf = Vec::new();
                            skip_parsing = true;
                        } else {
                            let mut nextpage_buf: Vec<u8> = Vec::new();
                            cur.read_to_end(&mut nextpage_buf).unwrap();
                            multipage_buf.append(&mut nextpage_buf);
                            if (_current_page + 1) != _total_pages {
                                skip_parsing = true;
                            } else {
                                parse_multipage = true;
                            }
                        }
                    }

                    /*
                    println!(
                        "-packet_id: {}, pages: {}/{}",
                        _packet_id, _current_page, _total_pages
                    );
                    */
                }
                //continue;

                last_packed_id = _packet_id;
                last_page = _current_page;

                if skip_parsing {
                    continue;
                }

                if parse_multipage {
                    cur = Cursor::new(&multipage_buf);
                }

                // get packet options and their byte size
                let mut opts = [0u8; 3];
                let mut szs = [0u16; 3];

                for x in 0..opts.len() {
                    opts[x] = cur.read_u8().unwrap();
                }

                for x in 0..szs.len() {
                    if opts[x] > 0 {
                        szs[x] = cur.read_u16::<LittleEndian>().unwrap();
                    }
                }

                // parse data block
                if opts[0] > 0 {
                    let mut vec_buffer = vec![0u8; szs[0].into()];
                    cur.read_exact(&mut vec_buffer).unwrap();
                    datapacket = tau_structs::DataPacket::from_bytes(
                        &vec_buffer,
                        &cached_mapping,
                        packetver,
                    );
                }

                // parse data_ext block
                if opts[1] > 0 {
                    let mut vec_buffer = vec![0u8; szs[1].into()];
                    cur.read_exact(&mut vec_buffer).unwrap();
                }

                // parse mapping block
                if opts[2] > 0 {
                    let mut vec_buffer = vec![0u8; szs[2].into()];
                    cur.read_exact(&mut vec_buffer).unwrap();

                    let mut mapping = HashMap::new();

                    let buf_txt = str::from_utf8(&vec_buffer).unwrap();
                    let buf_txt_split_lines = buf_txt.split("\n");
                    for sl in buf_txt_split_lines {
                        let buf_txt_split_val = sl.split("=");
                        let mut cur_val = 0;
                        let mut sens_id: u16 = 0;
                        let mut sens_mapping = String::new();
                        for sv in buf_txt_split_val {
                            if sv != "" {
                                if cur_val == 0 {
                                    let parse_sens_id_attempt = u16::from_str_radix(sv, 16); //sens_id
                                    match parse_sens_id_attempt {
                                        Ok(sens_id_parsed) => {
                                            sens_id = sens_id_parsed;
                                        }
                                        Err(_error) => break,
                                    };
                                } else if cur_val == 1 {
                                    sens_mapping = sv.trim_matches('"').to_string();
                                }
                                cur_val += 1;
                            }
                        }
                        if sens_id != 0 {
                            mapping.insert(sens_id, sens_mapping);
                        }
                    }
                    cached_mapping = mapping;
                }
            } else if packetver == 0 {
                println!("[ERROR!] Deprecated packet format")
            } else if packetver == -1 {
                //println!("[ERROR!] Unknown packet format")
            }
            //let execution_time = now.elapsed();

            //println!("{}", datapacket.to_string());

            let left_hand_input: InputData = Default::default();
            let right_hand_input: InputData = Default::default();
            let mut inputs = [left_hand_input, right_hand_input];

            let events_res = get_term_events();

            let time_now = Instant::now();

            match events_res {
                Ok(cur_event) => {
                    event_history.push((cur_event, time_now));
                }
                Err(_e) => {
                    //println!("{:?}", _e);
                }
            }
            /*
            for event_log in &event_history {
                let event_time = event_log.1;
                println!("{:?}", time_now - event_time > Duration::from_millis(1000));
            }
            */
            event_history.retain(|&x| time_now - x.1 < Duration::from_millis(600));

            for event_log in &event_history {
                let event = event_log.0;
                let event_time = event_log.1;

                // kinda ugly, this thing should be reworked
                if event_time == time_now {
                    if event == key_event_mod(KeyCode::Char('c'), KeyModifiers::CONTROL) {
                        panic!("EXIT: Ctrl+c")
                    }
                    if event == key_event(KeyCode::Esc) {
                        panic!("EXIT: Esc")
                    }
                    if event == key_event(KeyCode::Char('g')) {
                        print_controls();
                    }
                    if event == key_event(KeyCode::Char(' ')) {
                        if !gestures_enabled_bool {
                            println!("GESTURES OFF");
                        } else {
                            println!("GESTURES ON");
                        }
                        gestures_enabled_bool = !gestures_enabled_bool;
                    }
                    if event == key_event(KeyCode::Char('t')) {
                        print!("{esc}[2J{esc}[1;1H", esc = 27 as char);
                        print_inputs_bool = !print_inputs_bool;
                    }
                    if event == key_event(KeyCode::Char('b')) {
                        if !calibration_mode_bool {
                            println!("GRAB CALIBRATION MODE ON");
                        } else {
                            println!("GRAB CALIBRATION MODE OFF");
                        }

                        calibration_mode_bool = !calibration_mode_bool;
                    }
                }

                if event == key_event(KeyCode::Char('w')) {
                    inputs[0].joy_y = 1.0;
                }
                if event == key_event(KeyCode::Char('s')) {
                    inputs[0].joy_y = -1.0;
                }
                if event == key_event(KeyCode::Char('a')) {
                    inputs[0].joy_x = -1.0;
                }
                if event == key_event(KeyCode::Char('d')) {
                    inputs[0].joy_x = 1.0;
                }

                if event == key_event(KeyCode::Char('i')) {
                    inputs[1].joy_y = 1.0;
                }
                if event == key_event(KeyCode::Char('k')) {
                    inputs[1].joy_y = -1.0;
                }
                if event == key_event(KeyCode::Char('j')) {
                    inputs[1].joy_x = -1.0;
                }
                if event == key_event(KeyCode::Char('l')) {
                    inputs[1].joy_x = 1.0;
                }

                if event == key_event(KeyCode::Char('e')) {
                    inputs[0].a_btn = true;
                }
                if event == key_event(KeyCode::Char('r')) {
                    inputs[0].b_btn = true;
                }
                if event == key_event(KeyCode::Char('f')) {
                    inputs[0].joy_btn = true;
                }
                if event == key_event(KeyCode::Char('q')) {
                    inputs[0].trg_btn = true;
                }

                if event == key_event(KeyCode::Char('u')) {
                    inputs[1].a_btn = true;
                }
                if event == key_event(KeyCode::Char('y')) {
                    inputs[1].b_btn = true;
                }
                if event == key_event(KeyCode::Char('h')) {
                    inputs[1].joy_btn = true;
                }
                if event == key_event(KeyCode::Char('o')) {
                    inputs[1].trg_btn = true;
                }

                if event == key_event(KeyCode::Char('z')) {
                    inputs[0].grab = true;
                }
                if event == key_event(KeyCode::Char('x')) {
                    inputs[0].pinch = true;
                }
                if event == key_event(KeyCode::Char('c')) {
                    inputs[0].menu = true;
                }

                if event == key_event(KeyCode::Char(',')) {
                    inputs[1].grab = true;
                }
                if event == key_event(KeyCode::Char('m')) {
                    inputs[1].pinch = true;
                }
                if event == key_event(KeyCode::Char('n')) {
                    inputs[1].menu = true;
                }

                if event == key_event(KeyCode::Char('[')) {
                    inputs[0].calibrate = true;
                }
                if event == key_event(KeyCode::Char(']')) {
                    inputs[1].calibrate = true;
                }
            }

            if print_inputs_bool {
                stdout().execute(MoveTo(0, 0)).unwrap();
            }
            let map_hands = ["left_arm", "right_arm"];

            for h in 0..map_hands.len() {
                let map_hand = map_hands[h];

                let mut curl_vals = [0f32; 5];

                let hand_sensor =
                    datapacket.get_sensor_by_mapping(&format!("{}{}", map_hand, ".hand"));
                match hand_sensor {
                    Ok(module) => {
                        //let mut msg = String::new();
                        let mut hand_input = &mut inputs[h];

                        let mappings = [
                            &format!("{}{}", map_hand, ".hand.thumb"),
                            &format!("{}{}", map_hand, ".hand.index"),
                            &format!("{}{}", map_hand, ".hand.middle"),
                            &format!("{}{}", map_hand, ".hand.ring"),
                            &format!("{}{}", map_hand, ".hand.pinky"),
                        ];

                        let mod_q = module.quat();
                        let mod_q_inv = mod_q.invert();

                        for i in 0..mappings.len() {
                            let right_index_sensor = datapacket.get_sensor_by_mapping(mappings[i]);

                            match right_index_sensor {
                                Ok(sensor) => {
                                    let sens_q = sensor.quat();
                                    let diff = mod_q_inv * sens_q;
                                    if i == 0 {
                                        let proj = project_vector3_on_plane(
                                            diff * Vector3::unit_x(),
                                            Vector3::unit_z(),
                                        );
                                        let mut ud_angle = signed_angle_between_vector3(
                                            proj,
                                            Vector3::unit_x(),
                                            Vector3::unit_z(),
                                        );
                                        if h == 0 {
                                            ud_angle = -ud_angle;
                                        }
                                        ud_angle -= 30.0; // TODO: make configurable
                                        let curl_val = -ud_angle / 120.0; // TODO: make configurable
                                        curl_vals[i] = curl_val;
                                        //if ud_angle < 0.0 {
                                        hand_input.set_finger(i, curl_val);
                                        //}
                                    } else {
                                        let proj = project_vector3_on_plane(
                                            diff * Vector3::unit_x(),
                                            Vector3::unit_y(),
                                        );
                                        let mut ud_angle = signed_angle_between_vector3(
                                            proj,
                                            Vector3::unit_x(),
                                            Vector3::unit_y(),
                                        );
                                        if ud_angle > uv_snap_angle {
                                            ud_angle = -360.0 + ud_angle;
                                        }
                                        let curl_val = -ud_angle / 210.0;
                                        curl_vals[i] = curl_val;
                                        //if ud_angle < 0.0 {
                                        hand_input.set_finger(i, curl_val);
                                        //}
                                    }
                                }
                                Err(_) => (),
                            }
                        }

                        // Жесты
                        let l_cap = 0.30; // TODO: make configurable
                        let h_cap = 0.70; // TODO: make configurable

                        if curl_vals[2] > h_cap
                            && curl_vals[3] > h_cap
                            && curl_vals[4] > h_cap
                            && calibration_mode_bool
                        {
                            hand_input.calibrate = true;

                            // Средний, безымянный сжаты, мизинец расжат
                            // TODO: make configurable
                        }

                        if gestures_enabled_bool && !calibration_mode_bool {
                            // Кнопка триггера - указательный сжат
                            // TODO: make configurable
                            hand_input.trg_value = curl_vals[1] * 1.00;

                            if curl_vals[0] > 0.92 {
                                // TODO: make configurable
                                if h == 0 {
                                    hand_input.joy_y = 1.0;
                                } else {
                                    hand_input.joy_btn = true;
                                }
                            }

                            if curl_vals[1] > 0.80 {
                                //TODO: make configurable
                                hand_input.trg_btn = true;
                            }

                            /*
                            let tt = -0.1;
                            if curl_vals[0] < tt
                                && curl_vals[1] < tt
                                && curl_vals[2] < tt
                                && curl_vals[3] < tt
                                && curl_vals[4] < tt
                            {
                                hand_input.menu = true;
                            }
                            */

                            // Средний, безымянный, мизинец - сжаты
                            // TODO: make configurable
                            if curl_vals[2] > h_cap && curl_vals[3] > h_cap && curl_vals[4] > h_cap
                            {
                                hand_input.grab = true;

                            // Средний, безымянный сжаты, мизинец расжат
                            // TODO: make configurable
                            } else if curl_vals[2] > h_cap
                                && curl_vals[3] > h_cap
                                && curl_vals[4] < l_cap
                            {
                                hand_input.a_btn = true;
                                //hand_input.pinch = true;
                            }
                        }

                        if h == 0 {
                            //println!("L: {:?}", hand_input);
                            if print_inputs_bool {
                                print_inputs(&hand_input, "Left");
                            }

                            let stbytes: &[u8] = unsafe { any_as_u8_slice(hand_input) };
                            let write_at = left_hand_pipe.write(stbytes);
                            match write_at {
                                Ok(_) => {}
                                Err(e) => {
                                    println!("Left Pipe Err: {}", e);
                                }
                            }
                        } else {
                            //println!("R: {:?}", hand_input);
                            if print_inputs_bool {
                                print_inputs(&hand_input, "Right");
                            }

                            let stbytes: &[u8] = unsafe { any_as_u8_slice(hand_input) };
                            //println!("R: {:?}", stbytes);
                            let write_at = right_hand_pipe.write(stbytes);
                            match write_at {
                                Ok(_) => {}
                                Err(e) => {
                                    println!("Right Pipe Err: {}", e);
                                }
                            }
                        }
                    }
                    Err(_e) => (), //println!("{}", _e),
                }
            }
        }
    }
}
