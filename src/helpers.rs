use std::time::Duration;

use cgmath::{num_traits::signum, Deg, InnerSpace, Vector3};
use crossterm::{
    event::{poll, read, Event, KeyCode, KeyEvent, KeyModifiers},
    terminal::enable_raw_mode,
    Result,
};

use crate::opengloves_structs::InputData;

// Projects a vector onto a plane defined by a normal orthogonal to the plane.
// plane_normal should be unit vector!
// basically this https://docs.unity3d.com/ScriptReference/Vector3.ProjectOnPlane.html plus normalizing the result
pub fn project_vector3_on_plane(vector: Vector3<f32>, plane_normal: Vector3<f32>) -> Vector3<f32> {
    (vector - (vector.dot(plane_normal) * plane_normal)).normalize()
}

// Returns the angle in degrees between 'from' and 'to'
pub fn signed_angle_between_vector3(
    from: Vector3<f32>,
    to: Vector3<f32>,
    axis: Vector3<f32>,
) -> f32 {
    let unsigned_angle: Deg<f32> = from.angle(to).try_into().unwrap();
    let cross = from.cross(to);
    let sign = signum(axis.x * cross.x + axis.y * cross.y + axis.z * cross.z);
    unsigned_angle.0 * sign
}

// Convert anything to bytes. Used to convert OpenGloves InputData struct to bytes
// to send it through Named Pipes
pub unsafe fn any_as_u8_slice<T: Sized>(p: &T) -> &[u8] {
    ::std::slice::from_raw_parts((p as *const T) as *const u8, ::std::mem::size_of::<T>())
}

pub fn enable_raw_term() {
    enable_raw_mode().unwrap();
}

pub fn key_event(k: KeyCode) -> Event {
    Event::Key(KeyEvent::new(k, KeyModifiers::NONE))
}

pub fn key_event_mod(k: KeyCode, m: KeyModifiers) -> Event {
    Event::Key(KeyEvent::new(k, m))
}

pub fn get_term_events() -> Result<Event> {
    let poll_res = poll(Duration::from_millis(0));
    match poll_res {
        Ok(b) => {
            //println!("{}", b);
            if b {
                Ok(read().unwrap())
            } else {
                Err(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    "buffer empty",
                ))
            }
        }
        Err(e) => Err(e),
    }
}

pub fn print_inputs(input: &InputData, label: &str) {
    ptable!(
        [
            "Input", "Thumb", "Index", "Middle", "Ring", "Pinky", "JoyXY", "TRG", "Joy Btn",
            "Trg Btn", "A Btn", "B Btn", "Grab", "Pinch", "Menu", "Calib"
        ],
        [
            label,
            format!(
                "{:>+02.1} {:>+02.1} {:>+02.1}",
                &input.a0, &input.a1, &input.a2
            ),
            format!(
                "{:>+02.1} {:>+02.1} {:>+02.1}",
                &input.b1, &input.b2, &input.b3
            ),
            format!(
                "{:>+02.1} {:>+02.1} {:>+02.1}",
                &input.c1, &input.c2, &input.c3
            ),
            format!(
                "{:>+02.1} {:>+02.1} {:>+02.1}",
                &input.d1, &input.d2, &input.d2
            ),
            format!(
                "{:>+02.1} {:>+02.1} {:>+02.1}",
                &input.e1, &input.e2, &input.e3
            ),
            format!("{:>+02.1} {:>+02.1}", &input.joy_x, &input.joy_y),
            format!("{:>+02.1}", &input.trg_value),
            format!("{:<5}", &input.joy_btn),
            format!("{:<5}", &input.trg_btn),
            format!("{:<5}", &input.a_btn),
            format!("{:<5}", &input.b_btn),
            format!("{:<5}", &input.grab),
            format!("{:<5}", &input.pinch),
            format!("{:<5}", &input.menu),
            format!("{:<5}", &input.calibrate),
        ]
    );
}
