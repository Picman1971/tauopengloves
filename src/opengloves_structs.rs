extern crate libc;

// Struct to send to "\\.\pipe\vrapplication\input\glove\v2\<left/right>"
// As in v2 https://github.com/LucidVR/opengloves-driver/wiki/Driver-Input
#[derive(Default, Debug)]
#[repr(C)]
pub struct InputData {
    pub a0: libc::c_float,
    pub a1: libc::c_float,
    pub a2: libc::c_float,
    pub a3: libc::c_float,

    pub b0: libc::c_float,
    pub b1: libc::c_float,
    pub b2: libc::c_float,
    pub b3: libc::c_float,

    pub c0: libc::c_float,
    pub c1: libc::c_float,
    pub c2: libc::c_float,
    pub c3: libc::c_float,

    pub d0: libc::c_float,
    pub d1: libc::c_float,
    pub d2: libc::c_float,
    pub d3: libc::c_float,

    pub e0: libc::c_float,
    pub e1: libc::c_float,
    pub e2: libc::c_float,
    pub e3: libc::c_float,

    pub s0: libc::c_float,
    pub s1: libc::c_float,
    pub s2: libc::c_float,
    pub s3: libc::c_float,
    pub s4: libc::c_float,

    pub joy_x: libc::c_float,
    pub joy_y: libc::c_float,

    pub joy_btn: bool,
    pub trg_btn: bool,
    pub a_btn: bool,
    pub b_btn: bool,
    pub grab: bool,
    pub pinch: bool,
    pub menu: bool,
    pub calibrate: bool,

    pub trg_value: libc::c_float,
}

impl InputData {
    pub fn set_finger(&mut self, id: usize, v: f32) {
        match id {
            0 => {
                self.a0 = v;
                self.a1 = v;
                self.a2 = v;
                //self.a3 = v;
            }
            1 => {
                //self.b0 = v;
                self.b1 = v * 0.8;
                self.b2 = v * 0.6;
                self.b3 = v * 0.5;
            }
            2 => {
                //self.c0 = v;
                self.c1 = v * 0.8;
                self.c2 = v * 0.6;
                self.c3 = v * 0.5;
            }
            3 => {
                //self.d0 = v;
                self.d1 = v * 0.8;
                self.d2 = v * 0.6;
                self.d3 = v * 0.5;
            }
            4 => {
                //self.e0 = v;
                self.e1 = v * 0.8;
                self.e2 = v * 0.6;
                self.e3 = v * 0.5;
            }
            _ => unreachable!(),
        }
    }
}
