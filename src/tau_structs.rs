#![allow(dead_code, unused)]
use byteorder::{LittleEndian, ReadBytesExt};
use cgmath::Quaternion;
use std::collections::HashMap;
use std::io::Cursor;
use std::string::String;

pub struct Sensor {
    pub id: u16,
    pub mapping: String,
    pub active: bool,
    pub bad_coords: bool,
    pub q0: f32,
    pub q1: f32,
    pub q2: f32,
    pub q3: f32,
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl Sensor {
    pub fn quat(&self) -> Quaternion<f32> {
        Quaternion::new(self.q0, self.q1, self.q2, self.q3)
    }
}

pub struct Module {
    serial: u16,
    sensors_active: u8,
    data_integrity: u8,
    sensors: Vec<Sensor>,
}

pub struct DataPacket {
    module_count: u8,
    modules: Vec<Module>,
    module_serials: Vec<u16>,
}

impl DataPacket {
    pub fn empty() -> DataPacket {
        return DataPacket {
            module_count: 0,
            modules: Vec::new(),
            module_serials: Vec::new(),
        };
    }

    pub fn get_sensor_by_mapping(&self, map: &str) -> Result<&Sensor, String> {
        for module in &self.modules {
            for sensor in &module.sensors {
                if sensor.mapping == map {
                    return Ok(&sensor);
                }
            }
        }
        Err(format!("No sensors found for mapping: {}", map))
    }

    pub fn from_bytes(
        bytes: &[u8],
        mapping: &HashMap<u16, std::string::String>,
        packetver: i32,
    ) -> DataPacket {
        let mut cur = Cursor::new(bytes);

        let module_count = cur.read_u8().unwrap();

        let mut module_serials: Vec<u16> = Vec::new();
        let mut modules: Vec<Module> = Vec::new();

        for _m in 0..module_count {
            let module_serial = cur.read_u16::<LittleEndian>().unwrap();
            module_serials.push(module_serial);
            let mut sensors: Vec<Sensor> = Vec::new();
            let cur_module_sensors_active = cur.read_u8().unwrap();
            let cur_module_data_integrity = cur.read_u8().unwrap();
            for s in 0..6 {
                let cur_sensor_id = module_serial * 16 + s;
                let mut cur_sensor_mapping = String::new();
                if mapping.contains_key(&cur_sensor_id) {
                    cur_sensor_mapping = mapping.get(&cur_sensor_id).unwrap().clone();
                }
                let cur_sensor_active = (cur_module_sensors_active & (1 << s)) > 0;
                let cur_sensor_bad_coords = (cur_module_data_integrity & (1 << s)) > 0;

                let mut cur_sensor_q0: f32 = 0.0;
                let mut cur_sensor_q1: f32 = 0.0;
                let mut cur_sensor_q2: f32 = 0.0;
                let mut cur_sensor_q3: f32 = 0.0;
                let mut cur_sensor_x: f32 = 0.0;
                let mut cur_sensor_y: f32 = 0.0;
                let mut cur_sensor_z: f32 = 0.0;

                if cur_sensor_active {
                    cur_sensor_q0 = cur.read_f32::<LittleEndian>().unwrap();
                    cur_sensor_q1 = cur.read_f32::<LittleEndian>().unwrap();
                    cur_sensor_q2 = cur.read_f32::<LittleEndian>().unwrap();
                    cur_sensor_q3 = cur.read_f32::<LittleEndian>().unwrap();
                    cur_sensor_x = cur.read_f32::<LittleEndian>().unwrap();
                    cur_sensor_y = cur.read_f32::<LittleEndian>().unwrap();
                    cur_sensor_z = cur.read_f32::<LittleEndian>().unwrap();
                }

                let sensor = Sensor {
                    id: cur_sensor_id,
                    mapping: cur_sensor_mapping.to_string(),
                    active: cur_sensor_active,
                    bad_coords: cur_sensor_bad_coords,
                    q0: cur_sensor_q0,
                    q1: cur_sensor_q1,
                    q2: cur_sensor_q2,
                    q3: cur_sensor_q3,
                    x: cur_sensor_x,
                    y: cur_sensor_y,
                    z: cur_sensor_z,
                };
                sensors.push(sensor);
            }
            let module = Module {
                serial: module_serial,
                sensors_active: cur_module_sensors_active,
                data_integrity: cur_module_data_integrity,
                sensors,
            };
            modules.push(module);
        }

        DataPacket {
            module_count,
            modules,
            module_serials,
        }
    }

    pub fn to_string(&self) -> String {
        let mut readable_data = "".to_owned();
        readable_data.push_str(&format!("number of modules: {:2}\n", self.module_count));
        for module in self.modules.iter() {
            readable_data.push_str(&format!("module {:3x}:\n", module.serial));
            for sensor in module.sensors.iter() {
                if sensor.active {
                    readable_data.push_str(&format!("  sensor {:4x}:\n", sensor.id));
                    readable_data.push_str(&format!(
                        "    active: {:5}, bad_coords: {:5}, mapping: [{}]\n",
                        sensor.active, sensor.bad_coords, sensor.mapping
                    ));
                    readable_data.push_str(&format!(
                        "    pos({:>+08.3} {:>+08.3} {:>+08.3})",
                        sensor.x, sensor.y, sensor.z
                    ));
                    readable_data.push_str(&format!(
                        "; quat({:>+06.3} {:>+06.3} {:>+06.3} {:>+06.3})\n",
                        sensor.q0, sensor.q1, sensor.q2, sensor.q3
                    ));
                }
            }
        }
        return readable_data;
    }
}
